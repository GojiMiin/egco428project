package com.egco428.justnote.Activity

import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.egco428.justnote.Adapter.CustomColortagSpinnerAdapter
import com.egco428.justnote.Database.NotesDataSource
import com.egco428.justnote.Entities.ColorTagProvider
import com.egco428.justnote.Entities.MyNote
import com.egco428.justnote.R
import kotlinx.android.synthetic.main.activity_show_drawing.*
import java.io.IOException

class ActivityShowDrawing : AppCompatActivity() {

    private var datasourceShowDrawing : NotesDataSource? = null
    private var oldDrawNote: MyNote? = null
    private var selectedNewTag: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_drawing)

        datasourceShowDrawing = NotesDataSource(this)
        val colors = ColorTagProvider.getColorTag()
        val customSpinnerAdapter =
            CustomColortagSpinnerAdapter(
                this,
                colors
            )
        tagShowSpinner.adapter = customSpinnerAdapter

        tagShowSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>) {
                parent.setSelection(0)
            }
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedNewTag = ColorTagProvider.getColorName(position)
            }
        }

        oldDrawNote = intent.getSerializableExtra("drawing") as MyNote
        nameDrawTitle.setText(oldDrawNote!!.title)
        drawShowDateTime.setText(oldDrawNote!!.dateTime)

        when(oldDrawNote!!.colorNote){
            "White" -> tagShowSpinner.setSelection(0)
            "Red" -> tagShowSpinner.setSelection(1)
            "Blue" -> tagShowSpinner.setSelection(2)
            "Yellow" -> tagShowSpinner.setSelection(3)
            "Green" -> tagShowSpinner.setSelection(4)
        }

        try {
            val bitmap = BitmapFactory.decodeFile(oldDrawNote!!.imagePath)
            drawingShowView.setImageBitmap(bitmap)
            drawingShowView.setVisibility(View.VISIBLE)
        } catch (e: IOException){
            e.printStackTrace()
        }

        imageShowBack.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivityForResult(intent, RESULT_OK)
            finish()
        }

        drawShowSave.setOnClickListener {

            var getDrawName = nameDrawTitle.text.toString()
            saveEditDrawing(getDrawName, oldDrawNote!!)

        }
    }

    fun saveEditDrawing(editName: String, oldDraw: MyNote) {
        val newDrawing = MyNote()

        newDrawing!!.title = editName
        newDrawing!!.colorNote = selectedNewTag
        newDrawing!!.dateTime = oldDraw.dateTime
        newDrawing!!.isDraw = oldDraw.isDraw
        newDrawing!!.imagePath = oldDraw.imagePath
        newDrawing!!.id = oldDraw.id

        datasourceShowDrawing!!.open()
        datasourceShowDrawing!!.updateNote(newDrawing)

        datasourceShowDrawing!!.close()
        val intent = Intent(this, MainActivity::class.java)
        startActivityForResult(intent, RESULT_OK)
        finish()
    }

}