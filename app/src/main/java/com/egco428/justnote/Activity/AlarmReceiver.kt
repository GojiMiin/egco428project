package com.egco428.justnote.Activity

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.egco428.justnote.Entities.MyNote
import com.egco428.justnote.R

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val reminderNoteTitle = intent.getStringExtra("Title")
        val reminderNoteText = intent.getStringExtra("notetext")
        val reminderChannelID = intent.getIntExtra("ChannelID",0)

        val intentNoti = Intent(context, MainActivity::class.java)

        val pendingIntentToNoti = PendingIntent.getActivity(context, reminderChannelID, intentNoti,PendingIntent.FLAG_UPDATE_CURRENT)
        val msgAction = NotificationCompat.Action.Builder(R.drawable.ic_alarm, "View note", pendingIntentToNoti).build()

        val mBuilder = NotificationCompat.Builder(context, "100")
            .setSmallIcon(R.drawable.ic_notifications)
            .setContentTitle(reminderNoteTitle)
            .setContentText(reminderNoteText)
            .addAction(msgAction)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setWhen(System.currentTimeMillis())

        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(System.currentTimeMillis().toInt(), mBuilder.build())
    }
}