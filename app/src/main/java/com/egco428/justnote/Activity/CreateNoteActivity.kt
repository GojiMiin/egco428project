package com.egco428.justnote.Activity

import android.app.*
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.ContextWrapper
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.egco428.justnote.Adapter.CustomColortagSpinnerAdapter
import com.egco428.justnote.Database.MySQLiteHelper
import com.egco428.justnote.Database.NotesDataSource
import com.egco428.justnote.Entities.ColorTagProvider
import com.egco428.justnote.Entities.MyNote
import com.egco428.justnote.R
import kotlinx.android.synthetic.main.activity_create_note.*
import kotlinx.android.synthetic.main.layout_menu_btm.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class CreateNoteActivity : AppCompatActivity(){

    //init variable
    private var datasource : NotesDataSource? = null
    private var selectedPath: String? = null
    private var oldNote: MyNote? = null
    private var isInsert: Boolean = false
    var reminderCalendar: Calendar? = null
    var colorPicked = ""
    val REQ_CODE = 1234
    val PICK_IMAGE_REQUEST = 5678

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_note)

        //Get all colortag
        val colors = ColorTagProvider.getColorTag()

        //Open database
        datasource = NotesDataSource(this)
        datasource!!.open()

        //Setup adapter to spinner
        val customSpinnerAdapter =
            CustomColortagSpinnerAdapter(
                this,
                colors
            )
        colorAllPickers.adapter = customSpinnerAdapter
        colorAllPickers.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                colorPicked = ColorTagProvider.getColorName(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                parent.setSelection(0)
            }
        }

        //Setup Back button
        imageBack.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivityForResult(intent, RESULT_OK)
            finish()
        }

        //Setup Save button
        imageSave.setOnClickListener {
            checkInput()
        }

        //Setup Add image button
        layout_addImg.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(intent, "Select a Photo"),
                PICK_IMAGE_REQUEST
            )
        }

        //Setup Delete image button
        imageDeleteNote.setOnClickListener {
            imageNote.setImageBitmap(null)
            imageNote.visibility = View.GONE
            imageDeleteNote.visibility = View.GONE
            isInsert = false
        }

        //Setup Add reminder button
        layout_addReminder.setOnClickListener {
            showDateTimeDialog(timeReminderText)
        }

        //Setup Delete reminder button
        reminderDeleteBtn.setOnClickListener {
            timeReminderText.text =  "Add Reminder"
            reminderDeleteBtn.visibility = View.GONE

        }

        //Setup note date text
        textDateTime.setText(SimpleDateFormat("d MMM yyyy", Locale.getDefault()).format(Date()))

        //Check intent if updated
        if (intent.getBooleanExtra("isViewOrUpdate", false)){
            oldNote = intent.getSerializableExtra("note") as MyNote
            inputNoteTitle.setText(oldNote!!.title)
            textDateTime.setText(oldNote!!.dateTime)
            inputNote.setText(oldNote!!.noteText)
            timeReminderText.setText(oldNote!!.reminderTime)
            reminderDeleteBtn.visibility = View.VISIBLE
            if(timeReminderText.text.isNullOrEmpty()){
                timeReminderText.text = "Add Reminder"
                reminderDeleteBtn.visibility = View.GONE
            }
            if(!oldNote!!.imagePath.isNullOrEmpty()){
                try {
                    val bitmap = BitmapFactory.decodeFile(oldNote!!.imagePath)
                    imageNote.setImageBitmap(bitmap)
                    imageNote.setVisibility(View.VISIBLE)
                    imageDeleteNote.visibility = View.VISIBLE
                    isInsert = true
                } catch (e: IOException){
                    e.printStackTrace()
                }
            }
            when(oldNote!!.colorNote){
                "White" -> colorAllPickers.setSelection(0)
                "Red" -> colorAllPickers.setSelection(1)
                "Blue" -> colorAllPickers.setSelection(2)
                "Yellow" -> colorAllPickers.setSelection(3)
                "Green" -> colorAllPickers.setSelection(4)
            }
        }
    }

    //Date time fragment function
    private fun showDateTimeDialog(date_time_in: TextView) {
        val calendar = Calendar.getInstance()
        val dateSetListener =
            OnDateSetListener { view, year, month, dayOfMonth ->
                calendar[Calendar.YEAR] = year
                calendar[Calendar.MONTH] = month
                calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val timeSetListener: TimePickerDialog.OnTimeSetListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        calendar[Calendar.HOUR_OF_DAY] = hourOfDay
                        calendar[Calendar.MINUTE] = minute
                        val simpleDateFormat = SimpleDateFormat("d MMM yyyy HH:mm")
                        date_time_in.setText(simpleDateFormat.format(calendar.time))
                        reminderCalendar = calendar
                        reminderDeleteBtn.visibility = View.VISIBLE
                    }
                TimePickerDialog(
                    this, timeSetListener,
                    calendar[Calendar.HOUR_OF_DAY], calendar[Calendar.MINUTE], false
                ).show()
            }
        DatePickerDialog(
            this, dateSetListener,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH]
        ).show()

    }


    //Create Notification channel for application
    private fun createNotificationChannel() {
        val channelName = "com.egco428.justnote.Activity"
        val description = "Notification channel for Just Note"
        val importance = NotificationManager.IMPORTANCE_DEFAULT

        val channel = NotificationChannel("100", channelName, importance)
        channel.description = description

        var notificationManager = getSystemService(NotificationManager::class.java)
        notificationManager.createNotificationChannel(channel)
    }

    //Check input function
    private fun checkInput(){
        if(inputNoteTitle.text.isEmpty()){
            Toast.makeText(this, "Note title can't be empty", Toast.LENGTH_SHORT).show()
            return
        } else if (inputNote.text.isEmpty()){
            Toast.makeText(this, "Note text can't be empty", Toast.LENGTH_SHORT).show()
            return
        }
        saveNote()
    }

    //Save note function
    private fun saveNote(){
        val newnote = MyNote()
        var note : MyNote? = null
        val titleName = inputNoteTitle.text.toString()
        var noteImageSaveBitmap: Bitmap? = null
        var noteImagePath:Uri? = null

        if(isInsert == true) {
            noteImageSaveBitmap = (imageNote.getDrawable() as BitmapDrawable).bitmap
            noteImagePath = saveNoteImageTodevice(noteImageSaveBitmap!!, titleName)
            newnote.imagePath = noteImagePath.toString()
        } else {
            if(oldNote!=null && oldNote!!.imagePath != null){
                val fileToDel = File(oldNote!!.imagePath)
                val isDel = fileToDel.delete()
            }
            noteImagePath = null
            newnote.imagePath = noteImagePath
        }

        newnote.title = titleName
        newnote.dateTime = textDateTime.text.toString()
        newnote.noteText = inputNote.text.toString()
        newnote.colorNote = colorPicked

        if(reminderCalendar != null && timeReminderText.text!="Add Reminder"){

            newnote.reminderTime = timeReminderText.text.toString()
            val randomChannelId = System.currentTimeMillis().toInt()
            newnote.channelId = randomChannelId

            val intent = Intent(this, AlarmReceiver::class.java)
            intent.putExtra("Title",newnote.title)
            intent.putExtra("DateTime",newnote.dateTime)
            intent.putExtra("notetext",newnote.noteText)
            intent.putExtra("colorNote",newnote.colorNote)
            intent.putExtra("imagePath",newnote.imagePath)
            intent.putExtra("ChannelID", newnote.channelId!!)

            //Setup boardcast for notification
            val pendingIntent = PendingIntent.getBroadcast(this, newnote.channelId!!, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
            alarmManager.setExact(
                AlarmManager.RTC_WAKEUP,
                reminderCalendar!!.timeInMillis,
                pendingIntent
            );
            Log.d("cancel",newnote.channelId!!.toString())
            createNotificationChannel()
        } else {

            if(oldNote != null){
                val intent = Intent(this, AlarmReceiver::class.java)
                val pendingIntent = PendingIntent.getBroadcast(this, oldNote!!.channelId!!, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                pendingIntent.cancel()
                val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
                alarmManager.cancel(pendingIntent)
            }

        }

        if(oldNote != null){
            newnote.id = oldNote!!.id
            datasource!!.updateNote(newnote)
        }
        else {
            datasource!!.createNote(newnote)
        }

        datasource!!.close()
        val intent = Intent(this, MainActivity::class.java)
        startActivityForResult(intent, RESULT_OK)
        finish()
    }

    //Receive intent from image gallery
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data!= null && data.data != null){
            var fileUrl = data.data
            Log.d("url test", data.toString())
            try {
                val inputStream = contentResolver.openInputStream(fileUrl!!)
                val bitmap = BitmapFactory.decodeStream(inputStream)
                imageNote.setImageBitmap(bitmap)
                imageNote.setVisibility(View.VISIBLE)
                imageDeleteNote.visibility = View.VISIBLE
                isInsert = true

            } catch (e: IOException){
                e.printStackTrace()
            }
        }
    }

    //Save image to device
    private fun saveNoteImageTodevice(bitmap: Bitmap, drawName: String): Uri {
        val wrapper = ContextWrapper(applicationContext)
        var file = wrapper.getDir("images", Context.MODE_PRIVATE)
        file = File(file, drawName + ".jpg")
        try {
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException){
            e.printStackTrace()
        }
        return Uri.parse(file.absolutePath)
    }

    override fun onResume() {
        super.onResume()
        datasource!!.open()
    }

    override fun onPause() {
        super.onPause()
        datasource!!.close()
    }

    override fun onDestroy() {
        super.onDestroy()
        datasource!!.close()
    }
}