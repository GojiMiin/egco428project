package com.egco428.justnote.Activity

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_create_drawing.view.*

class DrawPanel(context: Context, attrs: AttributeSet): View(context, attrs) {

    private var color: Int = 1
    private var stroke: Int = 5

    //clear canvas
    //private val clearPaint = Paint()

    //stroke 5f
    private val paintBlack = Paint()
    private val paintRed = Paint()
    private val paintBlue = Paint()
    private val paintGreen = Paint()
    private val paintYellow = Paint()

    private var pathBlack = Path()
    private var pathRed = Path()
    private var pathBlue = Path()
    private var pathGreen = Path()
    private var pathYellow = Path()

    //stroke 15f
    private val paint15fBlack = Paint()
    private val paint15fRed = Paint()
    private val paint15fBlue = Paint()
    private val paint15fGreen = Paint()
    private val paint15fYellow = Paint()

    private var pathStroke15fBlack = Path()
    private var pathStroke15fRed = Path()
    private var pathStroke15fBlue = Path()
    private var pathStroke15fGreen = Path()
    private var pathStroke15fYellow = Path()

    //stroke 35f
    private val paint35fBlack = Paint()
    private val paint35fRed = Paint()
    private val paint35fBlue = Paint()
    private val paint35fGreen = Paint()
    private val paint35fYellow = Paint()

    private var pathStroke35fBlack = Path()
    private var pathStroke35fRed = Path()
    private var pathStroke35fBlue = Path()
    private var pathStroke35fGreen = Path()
    private var pathStroke35fYellow = Path()

    private var eventX: Float = 0f
    private var eventY: Float = 0f
    private var fingerDown = false
    //private val mask = BitmapFactory.decodeResource(resources, R.drawable.eraser)

    init {
        //clear canvas
        //clearPaint.setXfermode(PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        //stroke 5f
        paintBlack.isAntiAlias = true
        paintBlack.strokeWidth = 5f
        paintBlack.setColor(Color.BLACK)
        paintBlack.style = Paint.Style.STROKE
        paintBlack.strokeJoin = Paint.Join.ROUND

        paintRed.isAntiAlias = true
        paintRed.strokeWidth = 5f
        paintRed.setColor(Color.RED)
        paintRed.style = Paint.Style.STROKE
        paintRed.strokeJoin = Paint.Join.ROUND

        paintBlue.isAntiAlias = true
        paintBlue.strokeWidth = 5f
        paintBlue.setColor(Color.BLUE)
        paintBlue.style = Paint.Style.STROKE
        paintBlue.strokeJoin = Paint.Join.ROUND

        paintGreen.isAntiAlias = true
        paintGreen.strokeWidth = 5f
        paintGreen.setColor(Color.GREEN)
        paintGreen.style = Paint.Style.STROKE
        paintGreen.strokeJoin = Paint.Join.ROUND

        paintYellow.isAntiAlias = true
        paintYellow.strokeWidth = 5f
        paintYellow.setColor(Color.YELLOW)
        paintYellow.style = Paint.Style.STROKE
        paintYellow.strokeJoin = Paint.Join.ROUND

        //stroke 15f
        paint15fBlack.isAntiAlias = true
        paint15fBlack.strokeWidth = 15f
        paint15fBlack.setColor(Color.BLACK)
        paint15fBlack.style = Paint.Style.STROKE
        paint15fBlack.strokeJoin = Paint.Join.ROUND

        paint15fRed.isAntiAlias = true
        paint15fRed.strokeWidth = 15f
        paint15fRed.setColor(Color.RED)
        paint15fRed.style = Paint.Style.STROKE
        paint15fRed.strokeJoin = Paint.Join.ROUND

        paint15fBlue.isAntiAlias = true
        paint15fBlue.strokeWidth = 15f
        paint15fBlue.setColor(Color.BLUE)
        paint15fBlue.style = Paint.Style.STROKE
        paint15fBlue.strokeJoin = Paint.Join.ROUND

        paint15fGreen.isAntiAlias = true
        paint15fGreen.strokeWidth = 15f
        paint15fGreen.setColor(Color.GREEN)
        paint15fGreen.style = Paint.Style.STROKE
        paint15fGreen.strokeJoin = Paint.Join.ROUND

        paint15fYellow.isAntiAlias = true
        paint15fYellow.strokeWidth = 15f
        paint15fYellow.setColor(Color.YELLOW)
        paint15fYellow.style = Paint.Style.STROKE
        paint15fYellow.strokeJoin = Paint.Join.ROUND

        //stroke 35f
        paint35fBlack.isAntiAlias = true
        paint35fBlack.strokeWidth = 35f
        paint35fBlack.setColor(Color.BLACK)
        paint35fBlack.style = Paint.Style.STROKE
        paint35fBlack.strokeJoin = Paint.Join.ROUND

        paint35fRed.isAntiAlias = true
        paint35fRed.strokeWidth = 35f
        paint35fRed.setColor(Color.RED)
        paint35fRed.style = Paint.Style.STROKE
        paint35fRed.strokeJoin = Paint.Join.ROUND

        paint35fBlue.isAntiAlias = true
        paint35fBlue.strokeWidth = 35f
        paint35fBlue.setColor(Color.BLUE)
        paint35fBlue.style = Paint.Style.STROKE
        paint35fBlue.strokeJoin = Paint.Join.ROUND

        paint35fGreen.isAntiAlias = true
        paint35fGreen.strokeWidth = 35f
        paint35fGreen.setColor(Color.GREEN)
        paint35fGreen.style = Paint.Style.STROKE
        paint35fGreen.strokeJoin = Paint.Join.ROUND

        paint35fYellow.isAntiAlias = true
        paint35fYellow.strokeWidth = 35f
        paint35fYellow.setColor(Color.YELLOW)
        paint35fYellow.style = Paint.Style.STROKE
        paint35fYellow.strokeJoin = Paint.Join.ROUND
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        eventX = event!!.x
        eventY = event!!.y

        when(event.action){
            MotionEvent.ACTION_DOWN ->{
                fingerDown = true

                when (color) {
                    1 -> { //Black
                        when(stroke) {
                            5 -> pathBlack.moveTo(eventX,eventY)
                            15 -> pathStroke15fBlack.moveTo(eventX,eventY)
                            35 -> pathStroke35fBlack.moveTo(eventX,eventY)
                        }
                    }
                    2 -> { //Red
                        when(stroke) {
                            5 -> pathRed.moveTo(eventX, eventY)
                            15 -> pathStroke15fRed.moveTo(eventX,eventY)
                            35 -> pathStroke35fRed.moveTo(eventX,eventY)
                        }
                    }
                    3 -> { //Blue
                        when(stroke) {
                            5 -> pathBlue.moveTo(eventX, eventY)
                            15 -> pathStroke15fBlue.moveTo(eventX,eventY)
                            35 -> pathStroke35fBlue.moveTo(eventX,eventY)
                        }
                    }
                    4 -> { //Green
                        when(stroke) {
                            5 -> pathGreen.moveTo(eventX, eventY)
                            15 -> pathStroke15fGreen.moveTo(eventX,eventY)
                            35 -> pathStroke35fGreen.moveTo(eventX,eventY)
                        }
                    }
                    5 -> { //Yellow
                        when(stroke) {
                            5 -> pathYellow.moveTo(eventX,eventY)
                            15 -> pathStroke15fYellow.moveTo(eventX,eventY)
                            35 -> pathStroke35fYellow.moveTo(eventX,eventY)
                        }
                    }
                    else -> {
                        return true
                    }
                }
                return true
            }

            MotionEvent.ACTION_MOVE ->{
                when (color) {
                    1 -> { //Black
                        when(stroke) {
                            5 -> pathBlack.lineTo(eventX,eventY)
                            15 -> pathStroke15fBlack.lineTo(eventX,eventY)
                            35 -> pathStroke35fBlack.lineTo(eventX,eventY)
                        }
                    }
                    2 -> { //Red
                        when(stroke) {
                            5 -> pathRed.lineTo(eventX, eventY)
                            15 -> pathStroke15fRed.lineTo(eventX,eventY)
                            35 -> pathStroke35fRed.lineTo(eventX,eventY)
                        }
                    }
                    3 -> { //Blue
                        when(stroke) {
                            5 -> pathBlue.lineTo(eventX, eventY)
                            15 -> pathStroke15fBlue.lineTo(eventX,eventY)
                            35 -> pathStroke35fBlue.lineTo(eventX,eventY)
                        }
                    }
                    4 -> { //Green
                        when(stroke) {
                            5 -> pathGreen.lineTo(eventX, eventY)
                            15 -> pathStroke15fGreen.lineTo(eventX,eventY)
                            35 -> pathStroke35fGreen.lineTo(eventX,eventY)
                        }
                    }
                    5 -> { //Yellow
                        when(stroke) {
                            5 -> pathYellow.lineTo(eventX, eventY)
                            15 -> pathStroke15fYellow.lineTo(eventX,eventY)
                            35 -> pathStroke35fYellow.lineTo(eventX,eventY)
                        }
                    }
                    else -> {
                        return true
                    }

                }
            }

            MotionEvent.ACTION_UP ->{
                fingerDown = false
            }
            else -> return false
        }
        invalidate()
        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas!!.drawPath(pathBlack,paintBlack)
        canvas!!.drawPath(pathRed,paintRed)
        canvas!!.drawPath(pathBlue,paintBlue)
        canvas!!.drawPath(pathGreen,paintGreen)
        canvas!!.drawPath(pathYellow,paintYellow)

        //stroke 15f
        canvas!!.drawPath(pathStroke15fBlack,paint15fBlack)
        canvas!!.drawPath(pathStroke15fRed,paint15fRed)
        canvas!!.drawPath(pathStroke15fBlue,paint15fBlue)
        canvas!!.drawPath(pathStroke15fGreen,paint15fGreen)
        canvas!!.drawPath(pathStroke15fYellow,paint15fYellow)

        //stroke 35f
        canvas!!.drawPath(pathStroke35fBlack,paint35fBlack)
        canvas!!.drawPath(pathStroke35fRed,paint35fRed)
        canvas!!.drawPath(pathStroke35fBlue,paint35fBlue)
        canvas!!.drawPath(pathStroke35fGreen,paint35fGreen)
        canvas!!.drawPath(pathStroke35fYellow,paint35fYellow)



        when (color) {
            1 -> { //Black
                when(stroke) {
                    5 -> canvas.drawCircle(eventX,eventY,0F,paintBlack)
                    15 -> canvas.drawCircle(eventX,eventY,0F,paint15fBlack)
                    35 -> canvas.drawCircle(eventX,eventY,0F,paint35fBlack)
                }
            }
            2 -> { //Red
                when(stroke) {
                    5 -> canvas.drawCircle(eventX,eventY,0F,paintRed)
                    15 -> canvas.drawCircle(eventX,eventY,0F,paint15fRed)
                    35 -> canvas.drawCircle(eventX,eventY,0F,paint35fRed)
                }
            }
            3 -> { //Blue
                when(stroke) {
                    5 -> canvas.drawCircle(eventX,eventY,0F,paintBlue)
                    15 -> canvas.drawCircle(eventX,eventY,0F,paint15fBlue)
                    35 -> canvas.drawCircle(eventX,eventY,0F,paint35fBlue)
                }
            }
            4 -> { //Green
                when(stroke) {
                    5 -> canvas.drawCircle(eventX,eventY,0F,paintGreen)
                    15 -> canvas.drawCircle(eventX,eventY,0F,paint15fGreen)
                    35 -> canvas.drawCircle(eventX,eventY,0F,paint35fGreen)
                }
            }
            5 -> { //Yellow
                when(stroke) {
                    5 -> canvas.drawCircle(eventX,eventY,0F,paintYellow)
                    15 -> canvas.drawCircle(eventX,eventY,0F,paint15fYellow)
                    35 -> canvas.drawCircle(eventX,eventY,0F,paint35fYellow)
                }
            }
            else -> {
                pathBlack.reset()
                pathRed.reset()
                pathBlue.reset()
                pathGreen.reset()
                pathYellow.reset()

                pathStroke15fBlack.reset()
                pathStroke15fRed.reset()
                pathStroke15fBlue.reset()
                pathStroke15fGreen.reset()
                pathStroke15fYellow.reset()

                pathStroke35fBlack.reset()
                pathStroke35fRed.reset()
                pathStroke35fBlue.reset()
                pathStroke35fGreen.reset()
                pathStroke35fYellow.reset()

            }

        }
    }

    fun changeColor(colorCode:Int) {
        color = colorCode
    }

    fun changeStroke(strokeSize:Int) {
        stroke = strokeSize
    }

}