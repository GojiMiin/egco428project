package com.egco428.justnote.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.res.ResourcesCompat
import com.baoyz.swipemenulistview.SwipeMenu
import com.baoyz.swipemenulistview.SwipeMenuCreator
import com.baoyz.swipemenulistview.SwipeMenuItem
import com.baoyz.swipemenulistview.SwipeMenuListView
import com.egco428.justnote.Adapter.CustomFilterSpinnerAdapter
import com.egco428.justnote.Adapter.myNoteAdapter
import com.egco428.justnote.Database.NotesDataSource
import com.egco428.justnote.Entities.FilterProvider
import com.egco428.justnote.Entities.MyNote
import com.egco428.justnote.R
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.leinardi.android.speeddial.SpeedDialView
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {

    //init variable
    val REQ_CODE = 1234
    val REQUEST_CODE_UPDATE_NOTE = 5678
    val REQUEST_CODE_UPDATE_DRAW = 3333
    private var datasource : NotesDataSource? = null
    private var allNotes : MutableList<MyNote>? = null
    private var selectedFilter: String = ""
    lateinit var noteListView: myNoteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Find id spinner
        val filterSpin = findViewById<Spinner>(R.id.filterSpinner)
        val speedDialView = findViewById<SpeedDialView>(R.id.speedDial)
        val allFilterList = FilterProvider.getFilter()

        //Setup floating button
        speedDialView.addActionItem(
            SpeedDialActionItem.Builder(
                R.id.createNote,
                R.drawable.ic_text_icon
            )
                .setFabBackgroundColor(
                    ResourcesCompat.getColor(
                        getResources(),
                        R.color.colorFb,
                        getTheme()
                    )
                )
                .setLabel(getString(R.string.create_note))
                .setLabelColor(Color.WHITE)
                .setLabelBackgroundColor(
                    ResourcesCompat.getColor(
                        getResources(),
                        R.color.colorSearchBackground,
                        getTheme()
                    )
                )
                .setLabelClickable(false)
                .create()
        )
        speedDialView.addActionItem(
            SpeedDialActionItem.Builder(
                R.id.createDraw,
                R.drawable.ic_drawing_icon
            )
                .setFabBackgroundColor(
                    ResourcesCompat.getColor(
                        getResources(),
                        R.color.colorFb,
                        getTheme()
                    )
                )
                .setLabel(getString(R.string.create_draw))
                .setLabelColor(Color.WHITE)
                .setLabelBackgroundColor(
                    ResourcesCompat.getColor(
                        getResources(),
                        R.color.colorSearchBackground,
                        getTheme()
                    )
                )
                .setLabelClickable(false)
                .create()
        )
        speedDialView.setOnActionSelectedListener(SpeedDialView.OnActionSelectedListener { actionItem ->
            when (actionItem.id) {
                R.id.createNote -> {
                    val intent = Intent(this, CreateNoteActivity::class.java)
                    startActivityForResult(intent, REQ_CODE)
                    finish()
                }
                R.id.createDraw -> {
                    val intent = Intent(this, NewDrawPage::class.java)
                    startActivityForResult(intent, REQ_CODE)
                    finish()
                }

            }
            true
        })

        //Setup filter dropdown
        val customSpinnerAdapter =
            CustomFilterSpinnerAdapter(this, allFilterList)
        filterSpin.adapter = customSpinnerAdapter

        filterSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>) {
                parent.setSelection(0)
            }
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedFilter = FilterProvider.getFilterElement(position)
                allNotes!!.clear()
                Log.d("search_t",noteSearchView.query.toString())
                allNotes = datasource!!.searchNotes(noteSearchView.query.toString(),selectedFilter)
                noteListView.updateListView(allNotes!!)
                Log.d("selected", selectedFilter)
            }
        }

        //Setup Search bar
        noteSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                allNotes!!.clear()
                if (!query.isNullOrEmpty()) {
                    allNotes = datasource!!.searchNotes(query!!,selectedFilter)
                    noteListView.updateListView(allNotes!!)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        noteSearchView.setOnClickListener {
            noteSearchView.setIconified(false)
        }
        val searchCloseButtonId = noteSearchView.context.resources.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        );
        val closeButton = this.noteSearchView.findViewById(searchCloseButtonId) as ImageView
        closeButton.setOnClickListener {
            allNotes!!.clear()
            noteSearchView.setQuery("", false);
            noteSearchView.clearFocus();
            allNotes = datasource!!.searchNotes(noteSearchView.query.toString(),selectedFilter)
            noteListView.updateListView(allNotes!!)
        }

        //Setup SwipeListView
        setSwipeListView();
        NoteListView.setOnItemClickListener { adapterView, view, position, l ->
            val note = allNotes!!.get(position)

            if(note.isDraw == true){
                val drawShowIntent = Intent(this, ActivityShowDrawing::class.java)
                drawShowIntent.putExtra("drawing", note)
                startActivityForResult(drawShowIntent, REQUEST_CODE_UPDATE_DRAW)
            } else {
                val noteIntent = Intent(this, CreateNoteActivity::class.java)
                noteIntent.putExtra("isViewOrUpdate", true)
                noteIntent.putExtra("note", note)
                startActivityForResult(noteIntent, REQUEST_CODE_UPDATE_NOTE)
            }

        }
        NoteListView.setOnMenuItemClickListener(object : SwipeMenuListView.OnMenuItemClickListener {
            override fun onMenuItemClick(position: Int, menu: SwipeMenu?, index: Int): Boolean {
                val selectedNote = allNotes!!.get(position)
                if(!allNotes!!.get(position).imagePath.isNullOrEmpty()) {
                    val fileToDel = File(allNotes!!.get(position).imagePath)
                    val isDel = fileToDel.delete()
                }
                datasource!!.deleteNote(selectedNote)
                allNotes!!.clear()
                allNotes = datasource!!.searchNotes(noteSearchView.query.toString(),selectedFilter)

                noteListView.updateListView(allNotes!!)
                Log.d("noteleft", allNotes.toString())
                return true
            }
        })

        //Load Data
        loadDataIntoListView()
    }

    private fun loadDataIntoListView() {
        datasource = NotesDataSource(this)
        datasource!!.open()
        allNotes = datasource!!.getAllNotes()

        //Setup listView
        noteListView = myNoteAdapter(allNotes!!, this)
        NoteListView.adapter = noteListView
    }

    private fun setSwipeListView() {
        val creator = SwipeMenuCreator { menu ->
            val deleteItem = SwipeMenuItem(this@MainActivity)
            deleteItem.setBackground(R.drawable.background_delete_note)
            deleteItem.width = 170
            deleteItem.setIcon(R.drawable.ic_bin)
            deleteItem.title = "Delete"
            deleteItem.titleSize = 14
            deleteItem.titleColor = resources.getColor(R.color.colorWhite)
            menu.addMenuItem(deleteItem)
        }
        NoteListView.setMenuCreator(creator)
    }

    override fun onResume() {
        super.onResume()
        datasource!!.open()
    }

    override fun onPause() {
        super.onPause()
        datasource!!.close()
    }

    override fun onDestroy() {
        super.onDestroy()
        datasource!!.close()
    }

}