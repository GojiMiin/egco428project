package com.egco428.justnote.Activity


import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.egco428.justnote.Adapter.CustomColortagSpinnerAdapter
import com.egco428.justnote.Database.NotesDataSource
import com.egco428.justnote.Entities.ColorTagProvider
import com.egco428.justnote.Entities.MyNote
import com.egco428.justnote.R
import kotlinx.android.synthetic.main.activity_create_drawing.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class NewDrawPage: AppCompatActivity() {

    private var dataSource: NotesDataSource? = null
    private var selectedTag: String = ""
    private var currentSelected: Int = 1
    private var hisSelected: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_create_drawing)

        val draw = findViewById<DrawPanel>(R.id.drawPanel)
        val spinner: Spinner = findViewById(R.id.tagSpinner)
        val tag = ColorTagProvider.getColorTag()

        dataSource = NotesDataSource(this)
        dataSource!!.open()

        //set create drawing note time
        drawDateTime.setText(SimpleDateFormat("d MMM yyyy", Locale.getDefault()).format(Date()))

        //set selected color icon
        colorBlack.setBackgroundResource(R.drawable.background_pen_selected)
        colorBlack.setPadding(15,15,15,15)
        colorBlack.setImageResource(R.drawable.ic_pen_color)
        colorBlack.setColorFilter(getResources().getColor(R.color.colorPen1))

        //setup colortag dropdown
        val customSpinnerAdapter =
            CustomColortagSpinnerAdapter(this, tag)
        spinner.adapter = customSpinnerAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>) {
                parent.setSelection(0)
            }
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedTag = ColorTagProvider.getColorName(position)
            }
        }

        imageDrawBack.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivityForResult(intent, RESULT_OK)
            finish()
        }

        drawClear.setOnClickListener {
            draw.changeColor(6) //Change to Eraser
        }

        colorBlack.setOnClickListener {
            currentSelected = 1
            unselectOldColor(hisSelected, currentSelected) //change old selected color icon back
            selectedNewColor(hisSelected, currentSelected) //change new selected color icon
            hisSelected = 1
            draw.changeColor(1) //Black
        }

        colorRed.setOnClickListener {
            currentSelected = 2
            unselectOldColor(hisSelected, currentSelected)
            selectedNewColor(hisSelected, currentSelected)
            hisSelected = 2
            draw.changeColor(2) //Red
        }

        colorBlue.setOnClickListener {
            currentSelected = 3
            unselectOldColor(hisSelected, currentSelected)
            selectedNewColor(hisSelected, currentSelected)
            hisSelected = 3
            draw.changeColor(3) //Blue
        }

        colorGreen.setOnClickListener {
            currentSelected = 4
            unselectOldColor(hisSelected, currentSelected)
            selectedNewColor(hisSelected, currentSelected)
            hisSelected = 4
            draw.changeColor(4) //Green
        }

        colorYellow.setOnClickListener {
            currentSelected = 5
            unselectOldColor(hisSelected, currentSelected)
            selectedNewColor(hisSelected, currentSelected)
            hisSelected = 5
            draw.changeColor(5) //Yellow
        }

        drawClear.setOnClickListener {
            draw.changeColor(6) //clear canvas
        }

        stroke5f.setOnClickListener {
            draw.changeStroke(5) //stroke size = 5f
        }

        stroke15f.setOnClickListener {
            draw.changeStroke(15)
        }

        stroke35f.setOnClickListener {
            draw.changeStroke(35)
        }

        drawSave.setOnClickListener { //save canvas to device

            if(inputDrawTitle.text.isEmpty()){
                Toast.makeText(this, "Draw title can't be empty", Toast.LENGTH_SHORT).show()
            } else {
                val bmpFromView = saveCanvasScreenShot(draw)
                val setName = inputDrawTitle.text.toString()
                val finalPath = saveImageTodevice(bmpFromView, setName)
                saveDraw(setName, finalPath.toString())

            }
        }

    }

    private fun saveCanvasScreenShot(view: DrawPanel): Bitmap { //capture canvas as screenshot
        val screenView = view
        screenView.setBackgroundColor(Color.WHITE)
        screenView.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(screenView.drawingCache)
        screenView.isDrawingCacheEnabled = false
        return bitmap
    }

    private fun saveImageTodevice(bitmap: Bitmap, drawName: String): Uri {

        val wrapper = ContextWrapper(applicationContext)
        var file = wrapper.getDir("images", Context.MODE_PRIVATE)

        file = File(file, drawName + ".jpg")

        try {
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException){
            e.printStackTrace()
        }
        return Uri.parse(file.absolutePath)
    }

    private fun saveDraw(fileName: String, imgPath: String) { //save drawing note
        val myDraw = MyNote()
        myDraw!!.title = fileName
        myDraw!!.colorNote = selectedTag
        myDraw!!.dateTime = drawDateTime.text.toString()
        myDraw!!.imagePath = imgPath
        myDraw!!.isDraw = true
        Log.d("data", myDraw.toString())
        dataSource!!.createNote(myDraw)
        val intent = Intent(this, MainActivity::class.java)
        startActivityForResult(intent, RESULT_OK)
        finish()
    }

    private fun unselectOldColor(oldColor: Int, newColor: Int){
        if(oldColor != newColor){
            when(oldColor){
                1 -> {
                    colorBlack.setImageResource(R.drawable.ic_pen_color)
                    colorBlack.setBackgroundResource(0)
                    colorBlack.setPadding(0,0,0,0)
                    colorBlack.setColorFilter(getResources().getColor(R.color.colorPen1))
                }
                2 -> {
                    colorRed.setImageResource(R.drawable.ic_pen_color)
                    colorRed.setBackgroundResource(0)
                    colorRed.setPadding(0,0,0,0)
                    colorRed.setColorFilter(getResources().getColor(R.color.colorPen2))
                }
                3 -> {
                    colorBlue.setImageResource(R.drawable.ic_pen_color)
                    colorBlue.setBackgroundResource(0)
                    colorBlue.setPadding(0,0,0,0)
                    colorBlue.setColorFilter(getResources().getColor(R.color.colorPen3))
                }
                4 -> {

                    colorGreen.setImageResource(R.drawable.ic_pen_color)
                    colorGreen.setBackgroundResource(0)
                    colorGreen.setPadding(0,0,0,0)
                    colorGreen.setColorFilter(getResources().getColor(R.color.colorPen4))
                }
                5 -> {
                    colorYellow.setBackgroundResource(0)
                    colorYellow.setPadding(0,0,0,0)
                    colorYellow.setImageResource(R.drawable.ic_pen_color)
                    colorYellow.setColorFilter(getResources().getColor(R.color.colorPen5))
                }
            }
        }
    }

    private fun selectedNewColor(oldColor: Int, newColor: Int){
        if(oldColor != newColor){
            when(newColor){
                1 -> {
                    colorBlack.setBackgroundResource(R.drawable.background_pen_selected)
                    colorBlack.setPadding(15,15,15,15)
                    colorBlack.setImageResource(R.drawable.ic_pen_color_selected)
                    colorBlack.setColorFilter(getResources().getColor(R.color.colorPen1))
                }
                2 -> {
                    colorRed.setBackgroundResource(R.drawable.background_pen_selected)
                    colorRed.setPadding(15,15,15,15)
                    colorRed.setImageResource(R.drawable.ic_pen_color_selected)
                    colorRed.setColorFilter(getResources().getColor(R.color.colorPen2))
                }
                3 -> {
                    colorBlue.setBackgroundResource(R.drawable.background_pen_selected)
                    colorBlue.setPadding(15,15,15,15)
                    colorBlue.setImageResource(R.drawable.ic_pen_color_selected)
                    colorBlue.setColorFilter(getResources().getColor(R.color.colorPen3))
                }
                4 -> {
                    colorGreen.setBackgroundResource(R.drawable.background_pen_selected)
                    colorGreen.setPadding(15,15,15,15)
                    colorGreen.setImageResource(R.drawable.ic_pen_color_selected)
                    colorGreen.setColorFilter(getResources().getColor(R.color.colorPen4))
                }
                5 -> {
                    colorYellow.setBackgroundResource(R.drawable.background_pen_selected)
                    colorYellow.setPadding(15,15,15,15)
                    colorYellow.setImageResource(R.drawable.ic_pen_color_selected)
                    colorYellow.setColorFilter(getResources().getColor(R.color.colorPen5))
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dataSource!!.open()
    }

    override fun onPause() {
        super.onPause()
        dataSource!!.close()
    }

}
