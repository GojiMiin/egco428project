package com.egco428.justnote.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.egco428.justnote.Entities.colorTag
import com.egco428.justnote.R

class CustomColortagSpinnerAdapter (context: Context, colortagList: List<colorTag>) : BaseAdapter() {
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val useList = colortagList
    private val teContext = context

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val ih: ItemHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.colortag_spinner, parent, false)
            ih =
                ItemHolder(
                    view
                )
            view?.tag = ih
        } else {
            view = convertView
            ih = view.tag as ItemHolder
        }
        val id = teContext.resources.getIdentifier(useList.get(position).imgLink, "drawable", teContext.packageName)
        ih.img.setBackgroundResource(id)

        return view
    }

    override fun getItem(position: Int): Any {
        return useList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return useList.size
    }

    private class ItemHolder (row: View?){
        val img: ImageView

        init {
            img = row?.findViewById(R.id.colorTagView) as ImageView
        }
    }
}