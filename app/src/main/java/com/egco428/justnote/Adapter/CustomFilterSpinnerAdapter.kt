package com.egco428.justnote.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.egco428.justnote.Entities.Filter
import com.egco428.justnote.Entities.colorTag
import com.egco428.justnote.R

class CustomFilterSpinnerAdapter(context: Context, filterList: List<Filter>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val useFilterList = filterList

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        var ihFilter: ItemHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.filter_spinner, parent, false)
            ihFilter =
                ItemHolder(
                    view
                )
            view?.tag = ihFilter
        } else {
            view = convertView
            ihFilter = view.tag as ItemHolder
        }

        ihFilter.ListFilter.setText(useFilterList.get(position).filterName)

        return view
    }

    override fun getItem(position: Int): Any {
        return useFilterList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return useFilterList.size
    }

    private class ItemHolder (row: View?){
        var ListFilter: TextView

        init {
            ListFilter = row?.findViewById(R.id.filterChoiceView) as TextView
        }
    }

}