package com.egco428.justnote.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.egco428.justnote.Entities.MyNote
import com.egco428.justnote.R
import kotlinx.android.synthetic.main.activity_create_note.view.*
import kotlinx.android.synthetic.main.note_container2.view.*
import kotlinx.android.synthetic.main.note_container2.view.textDateTime

class myNoteAdapter(notes: MutableList<MyNote>, var context: Context): BaseAdapter() {

    var allNotes = notes

    override fun getCount(): Int {
        return allNotes!!.size
    }
    override fun getItemId(p0: Int): Long {
        return  p0.toLong()
    }
    override fun getItem(p0: Int): Any {
        return allNotes!![p0].id
    }
    override fun getView(p0: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val rowMain : View

        if(convertView == null) {
            val layoutInflator = LayoutInflater.from(viewGroup!!.context)
            rowMain = layoutInflator.inflate(R.layout.note_container2, viewGroup, false)
            val viewHolder = ViewHolder(
                rowMain.textTitle,
                rowMain.textDateTime,
                rowMain.textNote,
                rowMain.indicatorView
            )
            rowMain.tag = viewHolder
        } else {
            rowMain = convertView
        }

        var viewHolder = rowMain.tag as ViewHolder

        //load old data
        viewHolder.textNote.text = allNotes!![p0].noteText
        if(viewHolder.textNote.text.isNullOrEmpty()){
            viewHolder.textNote.visibility = View.GONE
            val layoutDateParams = viewHolder.textDateTime.layoutParams as LinearLayout.LayoutParams
            layoutDateParams.bottomMargin = dpToPx(16)
            viewHolder.colorNote.layoutParams.height = 80
        } else {
            viewHolder.textNote.visibility = View.VISIBLE
            val layoutDateParams = viewHolder.textDateTime.layoutParams as LinearLayout.LayoutParams
            layoutDateParams.bottomMargin = dpToPx(4)
            viewHolder.colorNote.layoutParams.height = 140
        }

        viewHolder.textTitle.text = allNotes!![p0].title
        viewHolder.textDateTime.text = allNotes!![p0].dateTime

        //load old color tag
        if(allNotes!![p0].colorNote.toString() == "White"){
            viewHolder.colorNote.setBackgroundResource(R.drawable.background_title_indicator_white)
        }else if(allNotes!![p0].colorNote.toString() == "Red"){
            viewHolder.colorNote.setBackgroundResource(R.drawable.background_title_indicator_red)
        }else if(allNotes!![p0].colorNote.toString() == "Blue"){
            viewHolder.colorNote.setBackgroundResource(R.drawable.background_title_indicator_blue)
        }else if(allNotes!![p0].colorNote.toString() == "Yellow"){
            viewHolder.colorNote.setBackgroundResource(R.drawable.background_title_indicator_yellow)
        }else if(allNotes!![p0].colorNote.toString() == "Green"){
            viewHolder.colorNote.setBackgroundResource(R.drawable.background_title_indicator_green)
        }

        notifyDataSetChanged()
        return rowMain

    }

    fun updateListView(notes: MutableList<MyNote>){
        allNotes = notes
        notifyDataSetChanged()
    }

    //Change dp to px function
    fun dpToPx(dpVal: Int):Int{
        val dpValue = dpVal // margin in dps
        val d = context.resources.displayMetrics.density
        val margin = (dpValue * d).toInt() // margin in pixels
        return margin
    }


    private class ViewHolder(
        val textTitle: TextView,
        val textDateTime: TextView,
        val textNote: TextView,
        val colorNote: View
    )


}