package com.egco428.justnote.Database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class MySQLiteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(database: SQLiteDatabase) {
        database.execSQL(DATABASE_CREATE)
    }
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        Log.w(MySQLiteHelper::class.java!!.name,
            "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data")
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS)
        onCreate(db)
    }
    companion object {
        //init column
        val TABLE_COMMENTS = "notes"
        val COLUMN_ID = "_id"
        val COLUMN_TITLE = "notetitle"
        val COLUMN_DATETIME = "datetime"
        val COLUMN_NOTETEXT = "notetext"
        val COLUMN_IMAGEPATH = "imagepath"
        val COLUMN_COLORNOTE = "colornote"
        val COLUMN_ISDRAW = "isdraw"
        val COLUMN_REMINDER = "reminder"
        val COLUMN_CHANNELID = "channelid"


        private val DATABASE_NAME = "notes.db"
        private val DATABASE_VERSION = 2
        // Database creation sql statement
        private val DATABASE_CREATE = ("create table "
                + TABLE_COMMENTS + "(" + COLUMN_ID
                + " integer primary key autoincrement, " + COLUMN_TITLE
                + " text, " + COLUMN_DATETIME
                + " text, " + COLUMN_NOTETEXT
                + " text, " + COLUMN_IMAGEPATH
                + " text, " + COLUMN_COLORNOTE
                + " text, " + COLUMN_REMINDER
                + " text, " + COLUMN_CHANNELID
                + " text, " + COLUMN_ISDRAW
                + " flag INTEGER DEFAULT 0);")
    }
}