package com.egco428.justnote.Database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.egco428.justnote.Database.MySQLiteHelper.Companion.COLUMN_COLORNOTE
import com.egco428.justnote.Database.MySQLiteHelper.Companion.COLUMN_DATETIME
import com.egco428.justnote.Database.MySQLiteHelper.Companion.COLUMN_ID
import com.egco428.justnote.Database.MySQLiteHelper.Companion.COLUMN_TITLE
import com.egco428.justnote.Entities.MyNote

class NotesDataSource(context: Context) {
    // Database fields
    private var database: SQLiteDatabase? = null
    private val dbHelper: MySQLiteHelper
    private val allColumns = arrayOf<String>(
        MySQLiteHelper.COLUMN_ID,
        MySQLiteHelper.COLUMN_TITLE,
        MySQLiteHelper.COLUMN_DATETIME,
        MySQLiteHelper.COLUMN_NOTETEXT,
        MySQLiteHelper.COLUMN_IMAGEPATH,
        MySQLiteHelper.COLUMN_COLORNOTE,
        MySQLiteHelper.COLUMN_ISDRAW,
        MySQLiteHelper.COLUMN_REMINDER,
        MySQLiteHelper.COLUMN_CHANNELID
    )

    init {
        dbHelper = MySQLiteHelper(context)
    }

    @Throws(SQLException::class)
    fun open() {
        database = dbHelper.getWritableDatabase()
    }
    fun close() {
        dbHelper.close()
    }
    //Get all note
    fun getAllNotes(): MutableList<MyNote>{
        val notes = ArrayList<MyNote>()
        val cursor = database!!.query(
            MySQLiteHelper.TABLE_COMMENTS,
            allColumns, null, null, null, null, null
        )
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            val note = cursorToNote(cursor)
            notes.add(note)
            cursor.moveToNext()
        }
        cursor.close()
        return notes
    }

    //Search note query
    fun searchNotes(name: String,filter: String): MutableList<MyNote>{
        val notes = ArrayList<MyNote>()
        if(filter.isEmpty()){
            val cursor = database!!.query(
                true,
                MySQLiteHelper.TABLE_COMMENTS,
                allColumns,
                COLUMN_TITLE.toString() + " LIKE ?",
                arrayOf<String>(name + "%"),
                null,
                null,
                null,
                null
            )
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                val note = cursorToNote(cursor)
                Log.d("note", note.toString())
                notes.add(note)
                cursor.moveToNext()
            }
            cursor.close()
            return notes
        } else {
            //Check each filter
            if(filter == "Name"){
                val cursor = database!!.query(
                    true,
                    MySQLiteHelper.TABLE_COMMENTS,
                    allColumns,
                    COLUMN_TITLE.toString() + " LIKE ?",
                    arrayOf<String>(name + "%"),
                    null,
                    null,
                    COLUMN_TITLE,
                    null
                )
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val note = cursorToNote(cursor)
                    Log.d("note", note.toString())
                    notes.add(note)
                    cursor.moveToNext()
                }
                cursor.close()
                return notes
            } else if (filter == "Date"){
                val cursor = database!!.query(
                    true,
                    MySQLiteHelper.TABLE_COMMENTS,
                    allColumns,
                    COLUMN_TITLE.toString() + " LIKE ?",
                    arrayOf<String>(name + "%"),
                    null,
                    null,
                    COLUMN_DATETIME,
                    null
                )
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val note = cursorToNote(cursor)
                    Log.d("note", note.toString())
                    notes.add(note)
                    cursor.moveToNext()
                }
                cursor.close()
                return notes
            } else if (filter == "Color"){
                val cursor = database!!.query(
                    true,
                    MySQLiteHelper.TABLE_COMMENTS,
                    allColumns,
                    COLUMN_TITLE.toString() + " LIKE ?",
                    arrayOf<String>(name + "%"),
                    null,
                    null,
                    COLUMN_COLORNOTE,
                    null
                )
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val note = cursorToNote(cursor)
                    Log.d("note", note.toString())
                    notes.add(note)
                    cursor.moveToNext()
                }
                cursor.close()
                return notes
            }
        }
        return notes
    }

    //Create note data in db
    fun createNote(note: MyNote) {
        val values = ContentValues()
        values.put(MySQLiteHelper.COLUMN_TITLE, note.title)
        values.put(MySQLiteHelper.COLUMN_DATETIME, note.dateTime)
        values.put(MySQLiteHelper.COLUMN_NOTETEXT, note.noteText)
        values.put(MySQLiteHelper.COLUMN_IMAGEPATH, note.imagePath)
        values.put(MySQLiteHelper.COLUMN_COLORNOTE, note.colorNote)
        values.put(MySQLiteHelper.COLUMN_ISDRAW, note.isDraw)
        values.put(MySQLiteHelper.COLUMN_REMINDER, note.reminderTime)
        values.put(MySQLiteHelper.COLUMN_CHANNELID, note.channelId)
        val insertId = database!!.insert(
            MySQLiteHelper.TABLE_COMMENTS, null,
            values
        )
        val cursor = database!!.query(
            MySQLiteHelper.TABLE_COMMENTS,
            allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null, null
        )
        cursor.moveToFirst()
        cursor.close()
    }

    //Update note data in db
    fun updateNote(note: MyNote) {
        val values = ContentValues()
        values.put(MySQLiteHelper.COLUMN_TITLE, note.title)
        values.put(MySQLiteHelper.COLUMN_DATETIME, note.dateTime)
        values.put(MySQLiteHelper.COLUMN_NOTETEXT, note.noteText)
        values.put(MySQLiteHelper.COLUMN_IMAGEPATH, note.imagePath)
        values.put(MySQLiteHelper.COLUMN_COLORNOTE, note.colorNote)
        values.put(MySQLiteHelper.COLUMN_ISDRAW, note.isDraw)
        values.put(MySQLiteHelper.COLUMN_REMINDER, note.reminderTime)
        values.put(MySQLiteHelper.COLUMN_CHANNELID, note.channelId)
        Log.d("note", values.toString())
        val whereargs = arrayOf(note.id.toString())

        val updateId = database!!.update(
            MySQLiteHelper.TABLE_COMMENTS,
            values,
            "$COLUMN_ID=?",
            whereargs
        )
        val cursor = database!!.query(
            MySQLiteHelper.TABLE_COMMENTS,
            allColumns, MySQLiteHelper.COLUMN_ID + " = " + updateId, null, null, null, null
        )
        cursor.moveToFirst()
        cursor.close()
    }


    //Delete note in db
    fun deleteNote(note: MyNote) {
        val id = note.id
        println("Comment deleted with id: " + id)
        database!!.delete(
            MySQLiteHelper.TABLE_COMMENTS, MySQLiteHelper.COLUMN_ID
                    + " = " + id, null
        )
    }

    //Read data from cursor
    private fun cursorToNote(cursor: Cursor): MyNote {
        val note = MyNote()
        note.id = cursor.getInt(0)
        note.title = cursor.getString(1)
        note.dateTime = cursor.getString(2)
        note.noteText = cursor.getString(3)
        note.imagePath = cursor.getString(4)
        note.colorNote = cursor.getString(5)
        note.isDraw = cursor.getInt(6).toBoolean()
        note.reminderTime = cursor.getString(7)
        note.channelId = cursor.getInt(8)
        return note
    }
    fun Int.toBoolean():Boolean = if (this==1) true else false;
}