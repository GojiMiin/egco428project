package com.egco428.justnote.Entities

object ColorTagProvider {
    private var allTag = ArrayList<colorTag>()

    fun getColorTag(): ArrayList<colorTag>{
        return allTag
    }

    fun getColorName(position: Int): String{
        return allTag[position].colorName
    }

    init {
        allTag.add(colorTag("White", "ic_colortag_white"))
        allTag.add(colorTag("Red", "ic_colortag_red"))
        allTag.add(colorTag("Blue", "ic_colortag_blue"))
        allTag.add(colorTag("Yellow", "ic_colortag_yellow"))
        allTag.add(colorTag("Green", "ic_colortag_green"))
    }
}