package com.egco428.justnote.Entities


object FilterProvider {
    private var allFilter = ArrayList<Filter>()

    fun getFilter(): ArrayList<Filter>{
        return allFilter
    }

    fun getFilterElement(position: Int): String{
        return allFilter[position].filterName.toString()
    }

    init {
        allFilter.add(Filter("Name"))
        allFilter.add(Filter("Date"))
        allFilter.add(Filter("Color"))
    }
}