package com.egco428.justnote.Entities

import java.io.Serializable

//Include serializable for send obj intent
class MyNote: Serializable{
    var id: Int = 0;
    var title: String? = null;
    var dateTime: String? = null;
    var noteText: String? = null;
    var imagePath: String? = null;
    var colorNote: String? = null;
    var isDraw: Boolean = false;
    var reminderTime: String? = null;
    var channelId: Int? = null
}